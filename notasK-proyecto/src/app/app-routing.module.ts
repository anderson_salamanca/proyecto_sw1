import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { GruposComponent } from './components/grupos/grupos.component';
import { CreaNotaComponent } from './components/crea-nota/crea-nota.component';



const routes: Routes = [
  {path: 'login',
  component: LoginComponent
},
{
  path: 'grupos',
  component: GruposComponent
},
{
  path: 'crea_nota',
  component: CreaNotaComponent
},
{
  path: '**',
  pathMatch: 'full',
  redirectTo: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
