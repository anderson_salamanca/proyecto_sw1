import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreaNotaComponent } from './components/crea-nota/crea-nota.component';
import { LoginComponent } from './components/login/login.component';
import { GruposComponent } from './components/grupos/grupos.component';
import { NavbarComponent } from './components/shere/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    CreaNotaComponent,
    LoginComponent,
    GruposComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
