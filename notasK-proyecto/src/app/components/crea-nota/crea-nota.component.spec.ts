import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreaNotaComponent } from './crea-nota.component';

describe('CreaNotaComponent', () => {
  let component: CreaNotaComponent;
  let fixture: ComponentFixture<CreaNotaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreaNotaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreaNotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
